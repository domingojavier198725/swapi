""" Utilizar la API disponible del proyecto SWAPI, con el fin de responder a las siguientes preguntas:
a) ¿En cuántas películas aparecen planetas cuyo clima sea árido?,
b) ¿Cuántos Wookies aparecen en la sexta película?,
c) ¿Cuál es el nombre de la aeronave más grande en toda la saga?"""

import requests

def allData(url):
    next = url
    data = []
    while next:
        res = requests.get(next)
        res = res.json()
        next = res['next']
        for item in res['results']:
            data.append(item)
    return data


if __name__ == '__main__':
    print('Planetas aridos')
    peliculas = allData('https://swapi.dev/api/films/')
    data = []
    clima = 'arid'
    for film in peliculas:
        for j in film['planets']:
            planet = requests.get(j).json()
            if planet['climate'] == clima:
                data.append(film['title'])
                break    
    print(data)

    print('Wookies que estan en la sexta pelicula')
    dataWookie = []
    wookie = requests.get('https://swapi.dev/api/species/?search=wookie').json()
    wookie = wookie['results'][0]['people']
    dataPeople = requests.get('https://swapi.dev/api/films/6/').json()
    dataPeople = dataPeople['characters']
    for item in dataPeople:
        for j in wookie:
            if item == j:
                dataWookie.append(j)
    print(dataWookie)
    print('La nave mas grande')
    url = 'https://swapi.dev/api/starships/'
    dataNaves = allData(url)
    bigWave = dataNaves[0]
    for item in dataNaves:
        if float(item['length'].replace(',', '')) > float(bigWave['length'].replace(',', '')):
            bigWave = item
    print(bigWave)

